//
//  AboutUSVC.h
//  
//
//  Created by Ashwin on 02/07/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "HttpConnection.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "ViewController.h"


@interface AboutUSVC : UIViewController<SWRevealViewControllerDelegate,MBProgressHUDDelegate>{
    MBProgressHUD *HUD;
}
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, nonatomic) IBOutlet UINavigationItem *navItem;
@property (weak, nonatomic) IBOutlet UITextView *textViewAbout;
@property(nonatomic,retain) NSMutableData *webResponseData;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideMenuBtn;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightSideMenuBtn;
@property(nonatomic,retain) NSUserDefaults *defaults;
@end
