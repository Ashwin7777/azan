//
//  AboutUSVC.m
//  
//
//  Created by Ashwin on 02/07/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "AboutUSVC.h"

@interface AboutUSVC ()

@end

@implementation AboutUSVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createBarBtns];
    
    //Reachability *reach = [Reachability reachabilityWithHostName:@"www.google.com"];
    //reach.reachableBlock
    
    //[reach startNotifier];
    //[self callApi];
    self.textViewAbout.text = [self getAboutUSData];
}

-(NSString *)getAboutUSData{
    self.defaults = [NSUserDefaults standardUserDefaults];
    NSString *val = [self.defaults valueForKey:@"LANG"];
    NSString *serviceStr = @"";
    if([val isEqualToString:@"Arab"]){
        NSString *path = [[NSBundle mainBundle] pathForResource:@"about_ar"
                                                         ofType:@"rtf"];
        serviceStr = [NSString stringWithContentsOfFile:path
                                               encoding:NSUTF8StringEncoding
                                                  error:NULL];
    }else if([val isEqualToString:@"Eng"]){
        NSString *path = [[NSBundle mainBundle] pathForResource:@"about_en"
                                                         ofType:@"rtf"];
        serviceStr = [NSString stringWithContentsOfFile:path
                                               encoding:NSASCIIStringEncoding
                                                  error:NULL];
    }else{
        NSString *path = [[NSBundle mainBundle] pathForResource:@"about_hi"
                                                         ofType:@"rtf"];
        serviceStr = [NSString stringWithContentsOfFile:path
                                               encoding:NSUTF8StringEncoding
                                                  error:NULL];
    }
    return serviceStr;
}

-(void)callApi{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.delegate = self;
    HUD.label.text = @"Loading";
    [HUD showAnimated:YES];
    
    NSString *lanSel = @"";
    NSString *lang = [[NSUserDefaults standardUserDefaults] valueForKey:@"LANG"];
    if([lang isEqualToString:@"Hind"]){
        lanSel = @"Hindi";
    }else if([lang isEqualToString:@"Arab"]){
        lanSel = @"Arabic";
    }else {
        lanSel = @"English";
    }
    NSString *url = [NSString stringWithFormat:@"http://www.azanintl.com/Service1.svc/ViewContent?Language=%@&Page=AboutUs",lanSel];
    NSLog(@"SUB CATEGORY %@",url);
    
    NSURLRequest *request = [NSURLRequest
                             requestWithURL:[NSURL URLWithString:url]];
    
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if( theConnection ) {
        self.webResponseData = [NSMutableData data];
    }else {
        NSLog(@"Some error occurred in Connection");
    }
}

-(void)createBarBtns{
    self.defaults = [NSUserDefaults standardUserDefaults];
    NSString *val = [self.defaults valueForKey:@"LANG"];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.delegate = self;
    
    if([val isEqualToString:@"Arab"]){
        //right side menu..
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [backButton setFrame:CGRectMake(0, 0, 22,17)];
        [ backButton setBackgroundImage:[UIImage imageNamed:@"reveal-icon.png"] forState:UIControlStateNormal];
        [backButton setTintColor:[UIColor whiteColor]];
        [backButton addTarget:self.revealViewController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithCustomView:backButton];
        self.navItem.rightBarButtonItem = back;
        
        //left home btn..
        UIButton *homeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [homeBtn setFrame:CGRectMake(0, 0, 30,25)];
        [ homeBtn setBackgroundImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
        [homeBtn setTintColor:[UIColor whiteColor]];
        [homeBtn addTarget:self action:@selector(moveToChangeLangPage) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *home = [[UIBarButtonItem alloc]initWithCustomView:homeBtn];
        self.navItem.leftBarButtonItem = home;
        
    }
    else{
        //left side menu..
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [backButton setFrame:CGRectMake(0, 0, 22,17)];
        [ backButton setBackgroundImage:[UIImage imageNamed:@"reveal-icon.png"] forState:UIControlStateNormal];
        [backButton setTintColor:[UIColor whiteColor]];
        [backButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithCustomView:backButton];
        self.navItem.leftBarButtonItem = back;
        
        //home btn
        UIButton *homeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [homeBtn setFrame:CGRectMake(0, 0, 30,25)];
        [ homeBtn setBackgroundImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
        [homeBtn setTintColor:[UIColor whiteColor]];
        [homeBtn addTarget:self action:@selector(moveToChangeLangPage) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *home = [[UIBarButtonItem alloc]initWithCustomView:homeBtn];
        self.navItem.rightBarButtonItem = home;
        
        
    }
    self.revealViewController.rearViewRevealWidth = 350;
    self.revealViewController.rightViewRevealWidth = 350;
    [self.navBar addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

-(void)moveToChangeLangPage{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:nil];
    ViewController *add =
    [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    
    [self presentViewController:add
                       animated:YES
                     completion:nil];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.webResponseData  appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"Some error in your Connection. Please try again.");
    [HUD hideAnimated:YES];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSLog(@"Received Bytes from server: %lu", (unsigned long)[self.webResponseData length]);
    [HUD hideAnimated:YES];
    NSError *error;
    NSDictionary *JSONDictionary = [NSJSONSerialization JSONObjectWithData:self.webResponseData
                                                     options: NSJSONReadingMutableContainers
                                                       error: &error];
    NSDictionary *aboutUsData = [JSONDictionary objectForKey:@"ViewContentResult"];
    int status = [[aboutUsData objectForKey:@"Status"] intValue];
    
    if(status == 1){
        self.textViewAbout.text = [aboutUsData objectForKey:@"Description"];
    }else{
        self.textViewAbout.text = @"Azan is an online logistics platform, which offers full logistics solution like online connecting with right logistics partner at right place and time,last mile,delivery reverse pickup and many more.";
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.delegate = self;
    self.defaults = [NSUserDefaults standardUserDefaults];
    NSString *val = [self.defaults valueForKey:@"LANG"];
    NSLog(@"the language selected %@",val);
    
    if ( revealViewController )
    {
        if([val isEqualToString:@"Arab"]){
            [self.sideMenuBtn setEnabled:NO];
            [self.sideMenuBtn setTintColor: [UIColor clearColor]];
            [self.rightSideMenuBtn setEnabled:YES];
            [self.rightSideMenuBtn setTintColor: [UIColor whiteColor]];
            
            [self.rightSideMenuBtn setTarget: self.revealViewController];
            self.revealViewController.rightViewRevealWidth = 350;
            [self.rightSideMenuBtn setAction: @selector( rightRevealToggle: )];
            [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        }else{
            [self.sideMenuBtn setEnabled:YES];
            [self.sideMenuBtn setTintColor:[UIColor whiteColor]];
            [self.rightSideMenuBtn setEnabled:NO];
            [self.rightSideMenuBtn setTintColor: [UIColor clearColor]];
            
            [self.sideMenuBtn setTarget: self.revealViewController];
            self.revealViewController.rearViewRevealWidth = 350;
            [self.sideMenuBtn setAction: @selector( revealToggle: )];
            [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        }
    }
}

#pragma mark state preservation / restoration

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Save what you need here
    
    [super encodeRestorableStateWithCoder:coder];
}


- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Restore what you need here
    
    [super decodeRestorableStateWithCoder:coder];
}


- (void)applicationFinishedRestoringState
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Call whatever function you need to visually restore
    [self customSetup];
}

- (BOOL)revealControllerPanGestureShouldBegin:(SWRevealViewController *)revealController{
    self.defaults = [NSUserDefaults standardUserDefaults];
    NSString *val = [self.defaults valueForKey:@"LANG"];
    NSLog(@"the language selected %@",val);
    
    if([val isEqualToString:@"Arab"]){
        if(revealController.frontViewPosition == FrontViewPositionLeft){
            return NO;
        }
    }
    return YES;
}
@end
