//
//  EmployeeVC.h
//  
//
//  Created by Ashwin on 02/07/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "LanguageManager.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "ViewController.h"
@import AVFoundation;
@import AVKit;

@interface EmployeeVC : UIViewController<UITextViewDelegate,SWRevealViewControllerDelegate,MBProgressHUDDelegate>{
    MBProgressHUD *HUD;
}

@property(nonatomic,retain) NSMutableData *webResponseData;
@property(nonatomic, retain) NSDictionary *responseDict;
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, nonatomic) IBOutlet UINavigationItem *navItem;

@property (weak, nonatomic) IBOutlet UIView *arabAddressView;
@property (weak, nonatomic) IBOutlet UIView *normalCommentView;
@property (weak, nonatomic) IBOutlet UIView *arabCommentView;
@property (weak, nonatomic) IBOutlet UILabel *registrationLbl;
@property (weak, nonatomic) IBOutlet UIView *normalAddressView;
@property (weak, nonatomic) IBOutlet UITextView *addressTextView;
@property (weak, nonatomic) IBOutlet UITextView *addressArabTextView;
@property (weak, nonatomic) IBOutlet UITextField *fullNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileNoTextField;
@property (weak, nonatomic) IBOutlet UITextField *workExpTextField;
@property (weak, nonatomic) IBOutlet UITextField *categoryTextField;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UITextView *commentArabTextView;
- (IBAction)registerNowClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (retain, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideMenuBtn;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightSideMenuBtn;
@property(nonatomic,retain) NSUserDefaults *defaults;
    
    @property (nonatomic, retain) UITextField *txtFullName;
    @property (nonatomic, retain) UITextField *txtMobileNo;
    @property (nonatomic, retain) UITextField *txtWorkExp;
    @property (nonatomic, retain) UITextField *txtCategory;
    @property (nonatomic, retain) UIButton *registBtn;
    @property (nonatomic, retain) UILabel *registerLBL;
    @property (nonatomic, retain) UITextView *txtViewComment;
    @property (nonatomic, retain) UIImageView *imgViewComment;
    @property (nonatomic, retain) UIView *viewForComment;
    @property (nonatomic, retain) UITextView *txtViewAddress;
    @property (nonatomic, retain) UIImageView *imgViewAddress;
    @property (nonatomic, retain) UIView *viewForAddress;
    
    
@end
