//
//  EmployeeVC.m
//  
//
//  Created by Ashwin on 02/07/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "EmployeeVC.h"
#import "Constants.h"

@interface EmployeeVC (){

    AVPlayerViewController *playerViewController;
    AVPlayer * player;
    NSURLConnection *theConnection1;
    NSURLConnection *theConnection2;
    NSURLConnection *theConnection3;
}

@end

@implementation EmployeeVC
#pragma mark - Controller Delegate
    
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createBarBtns];
    [self playVideo];
    
    self.defaults = [NSUserDefaults standardUserDefaults];
    NSString *val = [self.defaults valueForKey:@"LANG"];
    if([val isEqualToString:@"Arab"]){
        [self.sideMenuBtn setEnabled:NO];
        [self.sideMenuBtn setTintColor: [UIColor clearColor]];
        [self.rightSideMenuBtn setEnabled:YES];
        [self.rightSideMenuBtn setTintColor: [UIColor whiteColor]];
    }else{
        [self.sideMenuBtn setEnabled:YES];
        [self.sideMenuBtn setTintColor:[UIColor whiteColor]];
        [self.rightSideMenuBtn setEnabled:NO];
        [self.rightSideMenuBtn setTintColor: [UIColor clearColor]];
    }
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.delegate = self;
    HUD.label.text = @"Loading";
    //[HUD showAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [player seekToTime:CMTimeMake(0, 1)];
    [player pause];
}
    
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: YES];
    [self initialize];
}

#pragma mark - public methods
-(void)initialize{
    CGSize screenSize      = [[UIScreen mainScreen] bounds].size;
    CGFloat widthOfScreen  = screenSize.width;
    CGFloat heightOfScreen = screenSize.height;
    
    
    self.scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 66, widthOfScreen, heightOfScreen)];
    self.scrollView.backgroundColor=[UIColor clearColor];
    [self.scrollView setDelegate:self];
    [self.scrollView setShowsVerticalScrollIndicator:YES];
    
    UIImageView *imagename = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"avatar.png"]];
    imagename.frame = CGRectMake(0.0, 0.0, imagename.image.size.width+12.0, imagename.image.size.height);
    imagename.contentMode = UIViewContentModeCenter;
    
    UIImageView *imagePhone = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"phone.png"]];
    imagePhone.frame = CGRectMake(0.0, 0.0, imagePhone.image.size.width+12.0, imagePhone.image.size.height);
    imagePhone.contentMode = UIViewContentModeCenter;
    
    UIImageView *imageworkExp = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"phone.png"]];
    imageworkExp.frame = CGRectMake(0.0, 0.0, imageworkExp.image.size.width+12.0, imageworkExp.image.size.height);
    imageworkExp.contentMode = UIViewContentModeCenter;
    
    UIImageView *imageList = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"list.png"]];
    imageList.frame = CGRectMake(0.0, 0.0, imageList.image.size.width+12.0, imageList.image.size.height);
    imageList.contentMode = UIViewContentModeCenter;
    
    UIImageView *imageLocation = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"location.png"]];
    imageLocation.frame = CGRectMake(0.0, 0.0, imageLocation.image.size.width+12.0, imageLocation.image.size.height);
    imageLocation.contentMode = UIViewContentModeCenter;
    
    UIImageView *imageComment = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"comment.png"]];
    imageComment.frame = CGRectMake(0.0, 0.0, imageComment.image.size.width+12.0, imageComment.image.size.height);
    imageComment.contentMode = UIViewContentModeCenter;

    
    //check for arab lang as it is right aligned..
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *val = [defaults valueForKey:@"LANG"];
    
    //flexible width
    int flexWidth = widthOfScreen - 40;
    
    //flexible height w.r.t view height
    int flexHeight = self.view.frame.origin.y + 30;
    
    //increasing height w.r.t addyaxis
    int ADDYAXIS = 50;
    
    int labelXAxis = (widthOfScreen / 2)/ 2;
    
    self.registerLBL = [[UILabel alloc] initWithFrame:CGRectMake(labelXAxis, flexHeight, flexWidth, 18)];
    [self.registerLBL setTextColor:[UIColor whiteColor]];
    [self.registerLBL setFont:[UIFont boldSystemFontOfSize:15]];
    self.registerLBL.text = CustomLocalisedString(@"Employee Registration", @"Registration");
    
    flexHeight = self.registerLBL.frame.origin.y + 50;
    
    self.txtFullName = [[UITextField alloc] initWithFrame:CGRectMake(20, flexHeight, flexWidth, 30)];
    self.txtFullName.borderStyle = UITextBorderStyleRoundedRect;
    self.txtFullName.placeholder = CustomLocalisedString(@"Full Name", @"Full Name");
    
    self.txtMobileNo = [[UITextField alloc] initWithFrame:CGRectMake(self.txtFullName.frame.origin.x, self.txtFullName.frame.origin.y + ADDYAXIS, flexWidth, 30)];
    self.txtMobileNo.borderStyle = UITextBorderStyleRoundedRect;
    self.txtMobileNo.placeholder = CustomLocalisedString(@"MobileNo", @"MobileNo");
    
    self.txtWorkExp = [[UITextField alloc] initWithFrame:CGRectMake(self.txtFullName.frame.origin.x, self.txtMobileNo.frame.origin.y + ADDYAXIS, flexWidth, 30)];
    self.txtWorkExp.borderStyle = UITextBorderStyleRoundedRect;
    self.txtWorkExp.placeholder = CustomLocalisedString(@"Work Experience", @"Work Experience");
    
    self.txtCategory = [[UITextField alloc] initWithFrame:CGRectMake(self.txtFullName.frame.origin.x, self.txtWorkExp.frame.origin.y + ADDYAXIS, flexWidth, 30)];
    self.txtCategory.borderStyle = UITextBorderStyleRoundedRect;
    self.txtCategory.placeholder = CustomLocalisedString(@"Job Profession", @"Job Profession");
    
    //create Address View
    self.viewForAddress = [[UIView alloc] initWithFrame:CGRectMake(self.txtFullName.frame.origin.x, self.txtCategory.frame.origin.y + ADDYAXIS, flexWidth, 80)];
    [self.viewForAddress setBackgroundColor:[UIColor whiteColor]];
    
    if([val isEqualToString:@"Arab"]){
        self.txtViewAddress = [[UITextView alloc] initWithFrame:CGRectMake(5, 2, flexWidth - 40, 75)];
        [self.txtViewAddress setBackgroundColor:[UIColor clearColor]];
        [self.txtViewAddress setText:CustomLocalisedString(@"Address", @"Address")];
        [self.txtViewAddress setTextColor:[UIColor lightGrayColor]];
        [self.txtViewAddress setTextAlignment:NSTextAlignmentRight];
        [self.txtViewAddress setDelegate:self];
        [self.txtViewAddress setFont:[UIFont boldSystemFontOfSize:15]];
        [self.viewForAddress addSubview:self.txtViewAddress];
        
        self.imgViewAddress = [[UIImageView alloc] initWithFrame:CGRectMake(self.txtViewAddress.frame.origin.x + self.txtViewAddress.frame.size.width + 5, 2, 25,26)];
        [self.imgViewAddress setImage:[imageComment image]];
        [self.viewForAddress addSubview:self.imgViewAddress];
        
    }
    else{
        self.imgViewAddress = [[UIImageView alloc] initWithFrame:CGRectMake(5, 2, 25,26)];
        [self.imgViewAddress setImage:[imageComment image]];
        [self.viewForAddress addSubview:self.imgViewAddress];
        
        self.txtViewAddress = [[UITextView alloc] initWithFrame:CGRectMake(self.imgViewAddress.frame.origin.x + self.imgViewAddress.frame.size.width + 5, 2, flexWidth - 40, 75)];
        [self.txtViewAddress setBackgroundColor:[UIColor clearColor]];
        [self.txtViewAddress setText:CustomLocalisedString(@"Address", @"Address")];
        [self.txtViewAddress setTextColor:[UIColor lightGrayColor]];
        [self.txtViewAddress setDelegate:self];
        [self.txtViewAddress setFont:[UIFont systemFontOfSize:15]];
        [self.viewForAddress addSubview:self.txtViewAddress];
    }
    
    //create comment view..
    self.viewForComment = [[UIView alloc] initWithFrame:CGRectMake(self.txtFullName.frame.origin.x, self.viewForAddress.frame.origin.y + self.viewForAddress.frame.size.height + 20, flexWidth, 80)];
    [self.viewForComment setBackgroundColor:[UIColor whiteColor]];
    
    if([val isEqualToString:@"Arab"]){
        self.txtViewComment = [[UITextView alloc] initWithFrame:CGRectMake(5, 2, flexWidth - 40, 75)];
        [self.txtViewComment setBackgroundColor:[UIColor clearColor]];
        [self.txtViewComment setText:CustomLocalisedString(@"Comment", @"Comment")];
        [self.txtViewComment setTextColor:[UIColor lightGrayColor]];
        [self.txtViewComment setTextAlignment:NSTextAlignmentRight];
        [self.txtViewComment setDelegate:self];
        [self.txtViewComment setFont:[UIFont boldSystemFontOfSize:15]];
        [self.viewForComment addSubview:self.txtViewComment];
        
        self.imgViewComment = [[UIImageView alloc] initWithFrame:CGRectMake(self.txtViewComment.frame.origin.x + self.txtViewComment.frame.size.width + 5, 2, 25,26)];
        [self.imgViewComment setImage:[imageComment image]];
        [self.viewForComment addSubview:self.imgViewComment];
        
    }
    else{
        self.imgViewComment = [[UIImageView alloc] initWithFrame:CGRectMake(5, 2, 25,26)];
        [self.imgViewComment setImage:[imageComment image]];
        [self.viewForComment addSubview:self.imgViewComment];
        
        self.txtViewComment = [[UITextView alloc] initWithFrame:CGRectMake(self.imgViewComment.frame.origin.x + self.imgViewComment.frame.size.width + 5, 2, flexWidth - 40, 75)];
        [self.txtViewComment setBackgroundColor:[UIColor clearColor]];
        [self.txtViewComment setText:CustomLocalisedString(@"Comment", @"Comment")];
        [self.txtViewComment setTextColor:[UIColor lightGrayColor]];
        [self.txtViewComment setDelegate:self];
        [self.txtViewComment setFont:[UIFont systemFontOfSize:15]];
        [self.viewForComment addSubview:self.txtViewComment];
    }
    
    self.registBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.registBtn setBackgroundColor:[UIColor colorWithRed:(0/255) green:(86.0/255) blue:(13.0/255) alpha:1]];
    [self.registBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.registBtn addTarget:self
                         action:@selector(registerNowClicked:)
               forControlEvents:UIControlEventTouchUpInside];
    [self.registBtn setTitle:CustomLocalisedString(@"Register Now", @"Register Now") forState:UIControlStateNormal];
    self.registBtn.frame = CGRectMake(20, self.viewForComment.frame.origin.y + self.viewForComment.frame.size.height + 20, flexWidth, 40);
    
    if([val isEqualToString:@"Arab"]){
        [self.txtFullName setRightViewMode:UITextFieldViewModeAlways];
        [self.txtMobileNo setRightViewMode:UITextFieldViewModeAlways];
        [self.txtWorkExp setRightViewMode:UITextFieldViewModeAlways];
        [self.txtCategory setRightViewMode:UITextFieldViewModeAlways];
        
        self.txtFullName.rightView =imagename;
        self.txtMobileNo.rightView = imagePhone;
        self.txtWorkExp.rightView = imageworkExp;
        self.txtCategory.rightView = imageList;
        
        self.txtFullName.textAlignment = NSTextAlignmentRight;
        self.txtMobileNo.textAlignment = NSTextAlignmentRight;
        self.txtWorkExp.textAlignment = NSTextAlignmentRight;
        self.txtCategory.textAlignment = NSTextAlignmentRight;
        
    }else{
        [self.txtFullName setLeftViewMode:UITextFieldViewModeAlways];
        [self.txtMobileNo setLeftViewMode:UITextFieldViewModeAlways];
        [self.txtWorkExp setLeftViewMode:UITextFieldViewModeAlways];
        [self.txtCategory setLeftViewMode:UITextFieldViewModeAlways];
        
        
        self.txtFullName.leftView =imagename;
        self.txtMobileNo.leftView = imagePhone;
        self.txtWorkExp.leftView = imageworkExp;
        self.txtCategory.leftView = imageList;
    }
    
    [self.scrollView addSubview:self.registerLBL];
    [self.scrollView addSubview:self.txtFullName];
    [self.scrollView addSubview:self.txtMobileNo];
    [self.scrollView addSubview:self.txtWorkExp];
    [self.scrollView addSubview:self.txtCategory];
    [self.scrollView addSubview:self.viewForAddress];
    [self.scrollView addSubview:self.viewForComment];
    [self.scrollView addSubview:self.registBtn];

    
    int y = CGRectGetMaxY(((UIView*)[_scrollView.subviews lastObject]).frame);
    
    if(y>450)
        y = y + 200;
    
    CGSize addSize = CGSizeMake(widthOfScreen, y);
    self.scrollView.contentSize = addSize;
    
    [self.view addSubview:self.scrollView];
    

}
    
-(void)customSetup{
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.delegate = self;
    self.defaults = [NSUserDefaults standardUserDefaults];
    NSString *val = [self.defaults valueForKey:@"LANG"];
    NSLog(@"the language selected %@",val);
    
    if ( revealViewController )
    {
        if([val isEqualToString:@"Arab"]){
            [self.sideMenuBtn setEnabled:NO];
            [self.sideMenuBtn setTintColor: [UIColor clearColor]];
            [self.rightSideMenuBtn setEnabled:YES];
            [self.rightSideMenuBtn setTintColor: [UIColor whiteColor]];
            
            [self.rightSideMenuBtn setTarget: self.revealViewController];
            self.revealViewController.rightViewRevealWidth = 350;
            [self.rightSideMenuBtn setAction: @selector( rightRevealToggle: )];
            [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        }else{
            [self.sideMenuBtn setEnabled:YES];
            [self.sideMenuBtn setTintColor:[UIColor whiteColor]];
            [self.rightSideMenuBtn setEnabled:NO];
            [self.rightSideMenuBtn setTintColor: [UIColor clearColor]];
            
            [self.sideMenuBtn setTarget: self.revealViewController];
            self.revealViewController.rearViewRevealWidth = 350;
            [self.sideMenuBtn setAction: @selector( revealToggle: )];
            [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        }
    }
    
    [self.scrollView setBackgroundColor:[UIColor clearColor]];
}
    
-(NSString *)getLang{
    NSString *val = [[NSUserDefaults standardUserDefaults] valueForKey:@"LANG"];
    NSString *lang = @"";
    if([val isEqualToString:@"Hind"]){
        lang = @"Hindi";
    }else if([val isEqualToString:@"Arab"]){
        lang = @"Arabic";
    }else{
        lang = @"English";
    }
    return lang;
}

-(void)playVideo{
    NSURL *url1 = [[NSBundle mainBundle] URLForResource:VIDEONAME withExtension:@"mp4"];
    playerViewController = [[AVPlayerViewController alloc] init];
    
    AVURLAsset *asset = [AVURLAsset assetWithURL: url1];
    AVPlayerItem *item = [AVPlayerItem playerItemWithAsset: asset];
    
    player = [[AVPlayer alloc] initWithPlayerItem: item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    
    playerViewController.player = player;
    [playerViewController.view setFrame:CGRectMake(0, 0, self.videoView.bounds.size.width, self.videoView.bounds.size.height)];
    [playerViewController setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    playerViewController.showsPlaybackControls = NO;
    [self.videoView addSubview:playerViewController.view];
    
    [player play];
}

-(void)itemDidFinishPlaying:(NSNotification *) notification {
    // Will be called when AVPlayer finishes playing playerItem
    [self playVideo];
}

-(void)createBarBtns{
    self.defaults = [NSUserDefaults standardUserDefaults];
    NSString *val = [self.defaults valueForKey:@"LANG"];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.delegate = self;
    
    if([val isEqualToString:@"Arab"]){
        //right side menu..
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [backButton setFrame:CGRectMake(0, 0, 22,17)];
        [ backButton setBackgroundImage:[UIImage imageNamed:@"reveal-icon.png"] forState:UIControlStateNormal];
        [backButton setTintColor:[UIColor whiteColor]];
        [backButton addTarget:self.revealViewController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithCustomView:backButton];
        self.navItem.rightBarButtonItem = back;
        
        //left home btn..
        UIButton *homeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [homeBtn setFrame:CGRectMake(0, 0, 30,25)];
        [ homeBtn setBackgroundImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
        [homeBtn setTintColor:[UIColor whiteColor]];
        [homeBtn addTarget:self action:@selector(moveToChangeLangPage) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *home = [[UIBarButtonItem alloc]initWithCustomView:homeBtn];
        self.navItem.leftBarButtonItem = home;
        
    }
    else{
        //left side menu..
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [backButton setFrame:CGRectMake(0, 0, 22,17)];
        [ backButton setBackgroundImage:[UIImage imageNamed:@"reveal-icon.png"] forState:UIControlStateNormal];
        [backButton setTintColor:[UIColor whiteColor]];
        [backButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithCustomView:backButton];
        self.navItem.leftBarButtonItem = back;
        
        //home btn
        UIButton *homeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [homeBtn setFrame:CGRectMake(0, 0, 30,25)];
        [ homeBtn setBackgroundImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
        [homeBtn setTintColor:[UIColor whiteColor]];
        [homeBtn addTarget:self action:@selector(moveToChangeLangPage) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *home = [[UIBarButtonItem alloc]initWithCustomView:homeBtn];
        self.navItem.rightBarButtonItem = home;
        
        
    }
    self.revealViewController.rearViewRevealWidth = 350;
    self.revealViewController.rightViewRevealWidth = 350;
    [self.navBar addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

-(void)moveToChangeLangPage{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:nil];
    ViewController *add =
    [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    
    [self presentViewController:add
                       animated:YES
                     completion:nil];
}
    
-(void)callOTPApi:(NSString *)url andWithTag:(NSString *)tag{
    [HUD showAnimated:YES];
    
    NSLog(@"SUB CATEGORY %@",url);
    NSString *newStr = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"AFTER ADDING ESCAPE %@",newStr);
    
    NSURLRequest *request = [NSURLRequest
                             requestWithURL:[NSURL URLWithString:newStr]];
    
    if([tag isEqualToString:@"OTP"]){
        theConnection1 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if( theConnection1 ) {
            self.webResponseData = [NSMutableData data];
        }else {
            NSLog(@"Some error occurred in Connection");
        }
    }else if([tag isEqualToString:@"OTPVAL"]){
        theConnection2 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if( theConnection2 ) {
            self.webResponseData = [NSMutableData data];
        }else {
            NSLog(@"Some error occurred in Connection");
        }
    }else{
        theConnection3 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if( theConnection3 ) {
            self.webResponseData = [NSMutableData data];
        }else {
            NSLog(@"Some error occurred in Connection");
        }
    }
    
}
    
-(void)clearAllFields{
    self.txtFullName.text = @"";
    self.txtMobileNo.text = @"";
    self.txtViewAddress.text = @"Address";
    [self.txtViewAddress setTextColor:[UIColor lightGrayColor]];
    self.txtWorkExp.text = @"";
    self.txtCategory.text = @"";
    self.txtViewComment.text = @"Comment";
    [self.txtViewComment setTextColor:[UIColor lightGrayColor]];
}

#pragma mark state preservation / restoration
- (void)encodeRestorableStateWithCoder:(NSCoder *)coder{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Save what you need here
    
    [super encodeRestorableStateWithCoder:coder];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Restore what you need here
    
    [super decodeRestorableStateWithCoder:coder];
}

- (void)applicationFinishedRestoringState{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Call whatever function you need to visually restore
    [self customSetup];
}

#pragma mark - TextView Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView{
    if(textView == self.txtViewAddress){
        if ([textView.text isEqualToString:CustomLocalisedString(@"Address", @"Address")]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor]; //optional
        }
    }else{
        if ([textView.text isEqualToString:CustomLocalisedString(@"Comment", @"Comment")]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor]; //optional
        }
    }
    
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if(textView == self.txtViewAddress){
        if ([textView.text isEqualToString:@""]) {
            textView.text = CustomLocalisedString(@"Address", @"Address");
            textView.textColor = [UIColor lightGrayColor]; //optional
        }
    }else{
        if ([textView.text isEqualToString:@""]) {
            textView.text = CustomLocalisedString(@"Comment", @"Comment");
            textView.textColor = [UIColor lightGrayColor]; //optional
        }
    }
    
    [textView resignFirstResponder];
}

#pragma revealcontroller delegate
- (BOOL)revealControllerPanGestureShouldBegin:(SWRevealViewController *)revealController{
    self.defaults = [NSUserDefaults standardUserDefaults];
    NSString *val = [self.defaults valueForKey:@"LANG"];
    NSLog(@"the language selected %@",val);
    
    if([val isEqualToString:@"Arab"]){
        if(revealController.frontViewPosition == FrontViewPositionLeft){
            return NO;
        }
    }
    return YES;
}

#pragma mark - service related
- (IBAction)registerNowClicked:(id)sender {
    NSString *errorMsg = @"";
    if(self.txtFullName.text.length == 0 || self.txtFullName.text == NULL){
        errorMsg = @"Full name cannot be blank";
    }
    else if(self.txtMobileNo.text.length == 0 || self.txtMobileNo.text == NULL){
        errorMsg = @"Mobile No cannot be blank";
    }
    else if(self.txtWorkExp.text.length == 0 || self.txtWorkExp.text == NULL){
        errorMsg = @"Work Experience cannot be blank";
    }
    else if(self.txtCategory.text.length == 0 || self.txtCategory.text == NULL){
        errorMsg = @"Category cannot be blank";
    }
    else if(self.txtViewAddress.text.length == 0 || self.txtViewAddress.text == NULL || [self.txtViewAddress.text isEqualToString:@"Address"]){
        errorMsg = @"Address cannot be blank";
    }
    
    
    if(errorMsg.length == 0){
        //NSString *url = [NSString stringWithFormat:@"http://www.azanintl.com/Service1.svc/OtpGeneration?mobileNo=%@",self.mobileNoTextField.text];
        NSString *commentStr = self.commentTextView.text;
        if([self.txtViewComment.text isEqualToString:@"Comment"])
            commentStr = @"";
        
        NSString *url = [NSString stringWithFormat:@"http://www.azanintl.com/Service1.svc/EmployeeRegistration?Language=%@&FullName=%@&MobileNo=%@&WorkExperience=%@&Category=%@&Address=%@&Comment=%@",[self getLang],self.txtFullName.text,self.txtMobileNo.text,self.txtWorkExp.text,self.txtCategory.text,self.txtViewAddress.text,commentStr];
        [self callOTPApi:url andWithTag:@"REG"];
    }else{
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Employee" message:errorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - connection delegate
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.webResponseData  appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"Some error in your Connection. Please try again.");
    [HUD hideAnimated:YES];
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Employer" message:@"Issue with server, Please Try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    alert.tag = 6;
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    
    /*****
     check for space in parameters
     loading to be done 
     error if any show alert
     validations..
     *****/
    
    NSLog(@"Received Bytes from server: %lu", (unsigned long)[self.webResponseData length]);
    NSError *error;
    NSDictionary *JSONDictionary = [NSJSONSerialization JSONObjectWithData:self.webResponseData
                                                                   options: NSJSONReadingMutableContainers
                                                                     error: &error];
    [HUD hideAnimated:YES];
    NSLog(@"dictionary is %@",JSONDictionary);
    if(connection == theConnection1){
        self.responseDict = [JSONDictionary objectForKey:@"OtpGenerationResult"];
        BOOL flag = [[self.responseDict objectForKey:@"flag"] boolValue];
        if(flag){
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Hello!" message:@"Please enter OTP" delegate:self cancelButtonTitle:@"Verify" otherButtonTitles:nil];
            alert.tag = 1;
            alert.alertViewStyle = UIAlertViewStylePlainTextInput;
            UITextField * alertTextField = [alert textFieldAtIndex:0];
            alertTextField.keyboardType = UIKeyboardTypeNumberPad;
            //alertTextField.placeholder = @"Enter OTP";
            alertTextField.text = [self.responseDict objectForKey:@"Otp"];
            [alert show];
        }
    }else if(connection == theConnection2){
        self.responseDict = [JSONDictionary objectForKey:@"OtpVerificationResult"];
        BOOL flag = [[self.responseDict objectForKey:@"flag"] boolValue];
        if(flag){
            NSLog(@"the OTP VERified successfully");
            
            NSString *url = [NSString stringWithFormat:@"http://www.azanintl.com/Service1.svc/EmployeeRegistration?Language=%@&FullName=%@&MobileNo=%@&WorkExperience=%@&Category=%@&Address=%@&Comment=%@",[self getLang],self.txtFullName.text,self.txtMobileNo.text,self.txtWorkExp.text,self.txtCategory.text,self.txtViewAddress.text,self.txtViewComment.text];
            //[HUD showAnimated:YES];
            [self callOTPApi:url andWithTag:@"REG"];
            
        }else{
            NSLog(@"THE OTP FAILED");
        }
    }else{
        self.responseDict = [JSONDictionary objectForKey:@"EmployeeRegistrationResult"];
        int status = [[self.responseDict objectForKey:@"Status"] intValue];
        if(status == 1){
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Employee" message:@"Registered Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            alert.tag = 2;
            [alert show];
            [self clearAllFields];
        }
    }
}

#pragma mark - alertview delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 1){
        //[HUD showAnimated:YES];
        NSLog(@"Entered: %@",[[alertView textFieldAtIndex:0] text]);
    
        NSLog(@"the data is %@",self.responseDict);
    
        NSString *url = [NSString stringWithFormat:@"http://www.azanintl.com/Service1.svc/OtpVerification?mobileNo=%@&OtpCode=%@&userId=%@",self.txtMobileNo.text,[[alertView textFieldAtIndex:0] text],[self.responseDict objectForKey:@"tempUserId"]];
    
        [self callOTPApi:url andWithTag:@"OTPVAL"];
    }
}


@end
