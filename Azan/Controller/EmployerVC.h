//
//  EmployerVC.h
//  
//
//  Created by Ashwin on 02/07/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "LanguageManager.h"
#import "MBProgressHUD.h"
#import "ViewController.h"
#import "Constants.h"
@import AVFoundation;
@import AVKit;

@interface EmployerVC : UIViewController<UITextFieldDelegate,UITextViewDelegate,SWRevealViewControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,MBProgressHUDDelegate>{
    MBProgressHUD *HUD;
}

@property(nonatomic,retain) NSMutableData *webResponseData;
@property(nonatomic, retain) NSDictionary *responseDict;
@property (nonatomic,retain) NSMutableDictionary *countryDict;
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, nonatomic) IBOutlet UINavigationItem *navItem;

@property (weak, nonatomic) IBOutlet UIView *normalView;
@property (weak, nonatomic) IBOutlet UIView *viewForArab;
@property (weak, nonatomic) IBOutlet UILabel *registrationLbl;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideMenuBtn;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightSideMenuBtn;
@property (weak, nonatomic) IBOutlet UITextField *compNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *fullNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailIDTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileNoTextField;
@property (weak, nonatomic) IBOutlet UITextField *countryTextField;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UITextView *commentArabTextView;
- (IBAction)registerNowClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet UIButton *registerbtn;
@property (retain, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property(nonatomic,retain) NSUserDefaults *defaults;
@property(nonatomic, retain) UIPickerView *countrypicker;
    
    @property (nonatomic, retain) UITextField *txtFullName;
    @property (nonatomic, retain) UITextField *txtEmailID;
    @property (nonatomic, retain) UITextField *txtCompany;
    @property (nonatomic, retain) UITextField *txtMobileNo;
    @property (nonatomic, retain) UITextField *txtCountry;
    @property (nonatomic, retain) UIButton *registerBtn;
    @property (nonatomic, retain) UILabel *registerLBL;
    @property (nonatomic, retain) UITextView *txtViewComment;
    @property (nonatomic, retain) UIImageView *imgViewComment;
    @property (nonatomic, retain) UIView *viewForComment;
    

@end
