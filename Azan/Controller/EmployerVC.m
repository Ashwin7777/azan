//
//  EmployerVC.m
//
//
//  Created by Ashwin on 02/07/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "EmployerVC.h"
#import "Constants.h"

@interface EmployerVC (){
    AVPlayerViewController *playerViewController;
    AVPlayer * player;
    UIToolbar *toolBar;
    
    NSURLConnection *theConnection1;
    NSURLConnection *theConnection2;
    NSURLConnection *theConnection3;
    
}
    @property (nonatomic,retain)  NSArray *countryNames;
    @end

@implementation EmployerVC
    
#pragma mark - Controller Methods
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self initialize];
    
}
    
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self createBarBtns];
    [self playVideo];
    /*
     Bahrain = 973
     India = 91
     Iraq = 964
     Kuwait = 965
     Oman = 968
     Pakistan = 92
     Qatar = 974
     Syria = 963
     Saudi Arabia = 966
     UAE = 971
     Yemen = 967
     */
    //self.countryDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Bahrain",@"973",@"India",@"91",@"Iraq",@"964",@"Kuwait",@"965",@"Oman",@"968",@"Pakistan",@"92",@"Qatar",@"974",@"Syria",@"963",@"Saudi Arabia",@"966",@"UAE",@"971",@"Yemen",@"967", nil];
    self.countryDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Saudi Arabia",@"966",@"UAE",@"971",@"Qatar",@"974",@"Oman",@"968",@"Iraq",@"964",@"Jordan",@"123",@"Kuwait",@"965",@"Iran",@"456", nil];
    
    self.countryNames = [self.countryDict allValues];
    
    self.defaults = [NSUserDefaults standardUserDefaults];
    NSString *val = [self.defaults valueForKey:@"LANG"];
    if([val isEqualToString:@"Arab"]){
        [self.sideMenuBtn setEnabled:NO];
        [self.sideMenuBtn setTintColor: [UIColor clearColor]];
        [self.rightSideMenuBtn setEnabled:YES];
        [self.rightSideMenuBtn setTintColor: [UIColor whiteColor]];
    }else{
        [self.sideMenuBtn setEnabled:YES];
        [self.sideMenuBtn setTintColor:[UIColor whiteColor]];
        [self.rightSideMenuBtn setEnabled:NO];
        [self.rightSideMenuBtn setTintColor: [UIColor clearColor]];
    }
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.delegate = self;
    HUD.label.text = @"Loading";
}
    
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [player seekToTime:CMTimeMake(0, 1)];
    [player pause];
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
    
#pragma mark - Public methods
-(void)initialize{
    CGSize screenSize      = [[UIScreen mainScreen] bounds].size;
    CGFloat widthOfScreen  = screenSize.width;
    CGFloat heightOfScreen = screenSize.height;
    
    
    self.scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 66, widthOfScreen, heightOfScreen)];
    self.scrollView.backgroundColor=[UIColor clearColor];
    self.scrollView.showsHorizontalScrollIndicator = NO;
    [self.scrollView setDelegate:self];
    [self.scrollView setShowsVerticalScrollIndicator:NO];
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
    
    //images to show left or right of buttons..
    UIImageView *imagename = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"avatar.png"]];
    imagename.frame = CGRectMake(0.0, 0.0, imagename.image.size.width+12.0, imagename.image.size.height);
    imagename.contentMode = UIViewContentModeCenter;
    
    UIImageView *imagecomp = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"avatar.png"]];
    imagecomp.frame = CGRectMake(0.0, 0.0, imagecomp.image.size.width+12.0, imagecomp.image.size.height);
    imagecomp.contentMode = UIViewContentModeCenter;
    
    UIImageView *imagecountry = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"avatar.png"]];
    imagecountry.frame = CGRectMake(0.0, 0.0, imagecountry.image.size.width+12.0, imagecountry.image.size.height);
    imagecountry.contentMode = UIViewContentModeCenter;
    
    UIImageView *imagePhone = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"phone.png"]];
    imagePhone.frame = CGRectMake(0.0, 0.0, imagePhone.image.size.width+12.0, imagePhone.image.size.height);
    imagePhone.contentMode = UIViewContentModeCenter;
    
    UIImageView *imageEnvelope = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"envelope.png"]];
    imageEnvelope.frame = CGRectMake(0.0, 0.0, imageEnvelope.image.size.width+12.0, imageEnvelope.image.size.height);
    imageEnvelope.contentMode = UIViewContentModeCenter;
    
    UIImageView *imageComment = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"comment.png"]];
    imageComment.frame = CGRectMake(0.0, 0.0, imageComment.image.size.width+12.0, imageComment.image.size.height);
    imageComment.contentMode = UIViewContentModeCenter;
    
    //country picker view with done button..
    self.countrypicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
    [self.countrypicker setDataSource: self];
    [self.countrypicker setDelegate: self];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Done" style:UIBarButtonItemStyleDone
                                   target:self action:@selector(done)];
    toolBar = [[UIToolbar alloc]initWithFrame:
               CGRectMake(0, self.view.frame.size.height-
                          self.countrypicker.frame.size.height-100, 320, 50)];
    [toolBar setBarStyle:UIBarStyleBlackTranslucent];
    NSArray *toolbarItems = [NSArray arrayWithObjects:
                             doneButton, nil];
    [toolBar setItems:toolbarItems];
    self.countrypicker.showsSelectionIndicator = YES;
    //country picker view with done button..
    
    //get the language selected from the user..
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *val = [defaults valueForKey:@"LANG"];
    
    //flexible width
    int flexWidth = widthOfScreen - 40;
    
    //flexible height w.r.t view height
    int flexHeight = self.view.frame.origin.y + 30;
    
    //increasing height w.r.t addyaxis
    int ADDYAXIS = 50;
    
    int labelXAxis = (widthOfScreen / 2)/ 2;
    
    self.registerLBL = [[UILabel alloc] initWithFrame:CGRectMake(labelXAxis, flexHeight, flexWidth, 18)];
    [self.registerLBL setTextColor:[UIColor whiteColor]];
    [self.registerLBL setFont:[UIFont boldSystemFontOfSize:15]];
    self.registerLBL.text = CustomLocalisedString(@"Employer Registration", @"Registration");
    
    flexHeight = self.registerLBL.frame.origin.y + 50;
    
    self.txtFullName = [[UITextField alloc] initWithFrame:CGRectMake(20, flexHeight, flexWidth, 30)];
    self.txtFullName.borderStyle = UITextBorderStyleRoundedRect;
    self.txtFullName.placeholder = CustomLocalisedString(@"Full Name", @"Full Name");
    
    self.txtEmailID = [[UITextField alloc] initWithFrame:CGRectMake(self.txtFullName.frame.origin.x, self.txtFullName.frame.origin.y + ADDYAXIS, flexWidth, 30)];
    self.txtEmailID.borderStyle = UITextBorderStyleRoundedRect;
    self.txtEmailID.placeholder = CustomLocalisedString(@"EmailID", @"EmailID");
    
    self.txtCompany = [[UITextField alloc] initWithFrame:CGRectMake(self.txtFullName.frame.origin.x, self.txtEmailID.frame.origin.y + ADDYAXIS, flexWidth, 30)];
    self.txtCompany.borderStyle = UITextBorderStyleRoundedRect;
    self.txtCompany.placeholder = CustomLocalisedString(@"Company", @"Company");
    
    self.txtMobileNo = [[UITextField alloc] initWithFrame:CGRectMake(self.txtFullName.frame.origin.x, self.txtCompany.frame.origin.y + ADDYAXIS, flexWidth, 30)];
    self.txtMobileNo.borderStyle = UITextBorderStyleRoundedRect;
    self.txtMobileNo.placeholder = CustomLocalisedString(@"MobileNo", @"MobileNo");
    
    self.txtCountry = [[UITextField alloc] initWithFrame:CGRectMake(self.txtFullName.frame.origin.x, self.txtMobileNo.frame.origin.y + ADDYAXIS, flexWidth, 30)];
    self.txtCountry.borderStyle = UITextBorderStyleRoundedRect;
    self.txtCountry.placeholder = CustomLocalisedString(@"Country", @"Country");
    self.txtCountry.inputView = self.countrypicker;
    self.txtCountry.inputAccessoryView = toolBar;
    
    self.viewForComment = [[UIView alloc] initWithFrame:CGRectMake(self.txtFullName.frame.origin.x, self.txtCountry.frame.origin.y + ADDYAXIS, flexWidth, 80)];
    [self.viewForComment setBackgroundColor:[UIColor whiteColor]];
    
    if([val isEqualToString:@"Arab"]){
        self.txtViewComment = [[UITextView alloc] initWithFrame:CGRectMake(5, 2, flexWidth - 40, 75)];
        [self.txtViewComment setBackgroundColor:[UIColor clearColor]];
        [self.txtViewComment setText:CustomLocalisedString(@"Comment", @"Comment")];
        [self.txtViewComment setTextColor:[UIColor lightGrayColor]];
        [self.txtViewComment setTextAlignment:NSTextAlignmentRight];
        [self.txtViewComment setDelegate:self];
        [self.txtViewComment setFont:[UIFont boldSystemFontOfSize:15]];
        [self.viewForComment addSubview:self.txtViewComment];
        
        self.imgViewComment = [[UIImageView alloc] initWithFrame:CGRectMake(self.txtViewComment.frame.origin.x + self.txtViewComment.frame.size.width + 5, 2, 25,26)];
        [self.imgViewComment setImage:[imageComment image]];
        [self.viewForComment addSubview:self.imgViewComment];
        
    }else{
        self.imgViewComment = [[UIImageView alloc] initWithFrame:CGRectMake(5, 2, 25,26)];
        [self.imgViewComment setImage:[imageComment image]];
        [self.viewForComment addSubview:self.imgViewComment];
        
        self.txtViewComment = [[UITextView alloc] initWithFrame:CGRectMake(self.imgViewComment.frame.origin.x + self.imgViewComment.frame.size.width + 5, 2, flexWidth - 40, 75)];
        [self.txtViewComment setBackgroundColor:[UIColor clearColor]];
        [self.txtViewComment setText:CustomLocalisedString(@"Comment", @"Comment")];
        [self.txtViewComment setTextColor:[UIColor lightGrayColor]];
        [self.txtViewComment setDelegate:self];
        [self.txtViewComment setFont:[UIFont systemFontOfSize:15]];
        [self.viewForComment addSubview:self.txtViewComment];
    }
    
    self.registerBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.registerBtn setBackgroundColor:[UIColor colorWithRed:(0/255) green:(86.0/255) blue:(13.0/255) alpha:1]];
    [self.registerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.registerBtn addTarget:self
                         action:@selector(registerNowClicked:)
               forControlEvents:UIControlEventTouchUpInside];
    [self.registerBtn setTitle:CustomLocalisedString(@"Register Now", @"Register Now") forState:UIControlStateNormal];
    self.registerBtn.frame = CGRectMake(20, self.viewForComment.frame.origin.y + self.viewForComment.frame.size.height + 20, flexWidth, 40);
    
    if([val isEqualToString:@"Arab"]){
        [self.txtFullName setRightViewMode:UITextFieldViewModeAlways];
        [self.txtMobileNo setRightViewMode:UITextFieldViewModeAlways];
        [self.txtEmailID setRightViewMode:UITextFieldViewModeAlways];
        [self.txtCompany setRightViewMode:UITextFieldViewModeAlways];
        [self.txtCountry setRightViewMode:UITextFieldViewModeAlways];
        
        self.txtFullName.rightView =imagename;
        self.txtCompany.rightView = imagecomp;
        self.txtEmailID.rightView = imageEnvelope;
        self.txtMobileNo.rightView = imagePhone;
        self.txtCountry.rightView = imagecountry;
        
        self.txtFullName.textAlignment = NSTextAlignmentRight;
        self.txtCompany.textAlignment = NSTextAlignmentRight;
        self.txtEmailID.textAlignment = NSTextAlignmentRight;
        self.txtMobileNo.textAlignment = NSTextAlignmentRight;
        self.txtCountry.textAlignment = NSTextAlignmentRight;
        
    }else{
        [self.txtFullName setLeftViewMode:UITextFieldViewModeAlways];
        [self.txtMobileNo setLeftViewMode:UITextFieldViewModeAlways];
        [self.txtEmailID setLeftViewMode:UITextFieldViewModeAlways];
        [self.txtCompany setLeftViewMode:UITextFieldViewModeAlways];
        [self.txtCountry setLeftViewMode:UITextFieldViewModeAlways];
        
        self.txtFullName.leftView =imagename;
        self.txtCompany.leftView = imagecomp;
        self.txtEmailID.leftView = imageEnvelope;
        self.txtMobileNo.leftView = imagePhone;
        self.txtCountry.leftView = imagecountry;
        
    }
    
    [self.scrollView addSubview:self.registerLBL];
    [self.scrollView addSubview:self.txtFullName];
    [self.scrollView addSubview:self.txtEmailID];
    [self.scrollView addSubview:self.txtCompany];
    [self.scrollView addSubview:self.txtMobileNo];
    [self.scrollView addSubview:self.txtCountry];
    [self.scrollView addSubview:self.viewForComment];
    [self.scrollView addSubview:self.registerBtn];
    
    int y = CGRectGetMaxY(((UIView*)[_scrollView.subviews lastObject]).frame);
    
    if(y>450)
    y = y + 200;
    
    CGSize addSize = CGSizeMake(widthOfScreen, y);
    self.scrollView.contentSize = addSize;
    
    [self.view addSubview:self.scrollView];
}
    
-(void)done{
    self.txtCountry.text = [self.countryNames objectAtIndex:[self.countrypicker selectedRowInComponent:0]];
    [self.txtCountry endEditing:YES];
}
    
-(void)playVideo{
    NSURL *url1 = [[NSBundle mainBundle] URLForResource:VIDEONAME withExtension:@"mp4"];
    playerViewController = [[AVPlayerViewController alloc] init];
    
    AVURLAsset *asset = [AVURLAsset assetWithURL: url1];
    AVPlayerItem *item = [AVPlayerItem playerItemWithAsset: asset];
    
    player = [[AVPlayer alloc] initWithPlayerItem: item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    
    playerViewController.player = player;
    [playerViewController.view setFrame:CGRectMake(0, 0, self.videoView.bounds.size.width, self.videoView.bounds.size.height)];
    [playerViewController setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    playerViewController.showsPlaybackControls = NO;
    [self.videoView addSubview:playerViewController.view];
    
    [player play];
}
    
-(void)itemDidFinishPlaying:(NSNotification *) notification {
    // Will be called when AVPlayer finishes playing playerItem
    [self playVideo];
}
    
- (void)customSetup{
        SWRevealViewController *revealViewController = self.revealViewController;
        revealViewController.delegate = self;
        self.defaults = [NSUserDefaults standardUserDefaults];
        NSString *val = [self.defaults valueForKey:@"LANG"];
        NSLog(@"the language selected %@",val);
        
        if ( revealViewController )
        {
            if([val isEqualToString:@"Arab"]){
                [self.sideMenuBtn setEnabled:NO];
                [self.sideMenuBtn setTintColor: [UIColor clearColor]];
                [self.rightSideMenuBtn setEnabled:YES];
                [self.rightSideMenuBtn setTintColor: [UIColor whiteColor]];
                
                [self.rightSideMenuBtn setTarget: self.revealViewController];
                self.revealViewController.rightViewRevealWidth = 350;
                [self.rightSideMenuBtn setAction: @selector( rightRevealToggle: )];
                [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
            }else{
                [self.sideMenuBtn setEnabled:YES];
                [self.sideMenuBtn setTintColor:[UIColor whiteColor]];
                [self.rightSideMenuBtn setEnabled:NO];
                [self.rightSideMenuBtn setTintColor: [UIColor clearColor]];
                
                [self.sideMenuBtn setTarget: self.revealViewController];
                self.revealViewController.rearViewRevealWidth = 350;
                [self.sideMenuBtn setAction: @selector( revealToggle: )];
                [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
            }
        }
    }
    
-(void)createBarBtns{
    self.defaults = [NSUserDefaults standardUserDefaults];
    NSString *val = [self.defaults valueForKey:@"LANG"];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.delegate = self;
    
    if([val isEqualToString:@"Arab"]){
        //right side menu..
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [backButton setFrame:CGRectMake(0, 0, 22,17)];
        [ backButton setBackgroundImage:[UIImage imageNamed:@"reveal-icon.png"] forState:UIControlStateNormal];
        [backButton setTintColor:[UIColor whiteColor]];
        [backButton addTarget:self.revealViewController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithCustomView:backButton];
        self.navItem.rightBarButtonItem = back;
        
        //left home btn..
        UIButton *homeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [homeBtn setFrame:CGRectMake(0, 0, 30,25)];
        [ homeBtn setBackgroundImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
        [homeBtn setTintColor:[UIColor whiteColor]];
        [homeBtn addTarget:self action:@selector(moveToChangeLangPage) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *home = [[UIBarButtonItem alloc]initWithCustomView:homeBtn];
        self.navItem.leftBarButtonItem = home;
        
    }
    else{
        //left side menu..
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [backButton setFrame:CGRectMake(0, 0, 22,17)];
        [ backButton setBackgroundImage:[UIImage imageNamed:@"reveal-icon.png"] forState:UIControlStateNormal];
        [backButton setTintColor:[UIColor whiteColor]];
        [backButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithCustomView:backButton];
        self.navItem.leftBarButtonItem = back;
        
        //home btn
        UIButton *homeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [homeBtn setFrame:CGRectMake(0, 0, 30,25)];
        [ homeBtn setBackgroundImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
        [homeBtn setTintColor:[UIColor whiteColor]];
        [homeBtn addTarget:self action:@selector(moveToChangeLangPage) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *home = [[UIBarButtonItem alloc]initWithCustomView:homeBtn];
        self.navItem.rightBarButtonItem = home;
        
        
    }
    self.revealViewController.rearViewRevealWidth = 350;
    self.revealViewController.rightViewRevealWidth = 350;
    [self.navBar addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}
    
-(void)moveToChangeLangPage{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:nil];
    ViewController *add =
    [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    
    [self presentViewController:add
                       animated:YES
                     completion:nil];
}
    
-(NSString *)getLang{
    NSString *val = [[NSUserDefaults standardUserDefaults] valueForKey:@"LANG"];
    NSString *lang = @"";
    if([val isEqualToString:@"Hind"]){
        lang = @"Hindi";
    }else if([val isEqualToString:@"Arab"]){
        lang = @"Arabic";
    }else{
        lang = @"English";
    }
    return lang;
}
    
-(void)callOTPApi:(NSString *)url andWithTag:(NSString *)tag{
    [HUD showAnimated:YES];
    NSLog(@"SUB CATEGORY %@",url);
    NSString *newStr = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"AFTER ADDING ESCAPE %@",newStr);
    
    NSURLRequest *request = [NSURLRequest
                             requestWithURL:[NSURL URLWithString:newStr]];
    
    if([tag isEqualToString:@"OTP"]){
        theConnection1 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if( theConnection1 ) {
            self.webResponseData = [NSMutableData data];
        }else {
            NSLog(@"Some error occurred in Connection");
        }
    }else if([tag isEqualToString:@"OTPVAL"]){
        theConnection2 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if( theConnection2 ) {
            self.webResponseData = [NSMutableData data];
        }else {
            NSLog(@"Some error occurred in Connection");
        }
    }else{
        theConnection3 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if( theConnection3 ) {
            self.webResponseData = [NSMutableData data];
        }else {
            NSLog(@"Some error occurred in Connection");
        }
    }
    
}
    
-(void)clearAllFields{
    self.txtFullName.text = @"";
    self.txtMobileNo.text = @"";
    self.txtCompany.text = @"";
    self.txtEmailID.text = @"";
    self.txtCountry.text = @"";
    self.txtViewComment.text = @"Comment";
    [self.txtViewComment setTextColor:[UIColor lightGrayColor]];
}
    
#pragma mark state preservation / restoration
- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
    {
        NSLog(@"%s", __PRETTY_FUNCTION__);
        
        // Save what you need here
        
        [super encodeRestorableStateWithCoder:coder];
    }
    
    
- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
    {
        NSLog(@"%s", __PRETTY_FUNCTION__);
        
        // Restore what you need here
        
        [super decodeRestorableStateWithCoder:coder];
    }
    
    
- (void)applicationFinishedRestoringState
    {
        NSLog(@"%s", __PRETTY_FUNCTION__);
        
        // Call whatever function you need to visually restore
        [self customSetup];
    }
    
#pragma mark - Textview and Textfield Delegates
- (void)textViewDidBeginEditing:(UITextView *)textView
    {
        if ([textView.text isEqualToString:CustomLocalisedString(@"Comment", @"Comment")]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor]; //optional
        }
        [textView becomeFirstResponder];
    }
    
- (void)textViewDidEndEditing:(UITextView *)textView
    {
        if ([textView.text isEqualToString:@""]) {
            textView.text = CustomLocalisedString(@"Comment", @"Comment");
            textView.textColor = [UIColor lightGrayColor]; //optional
        }
        [textView resignFirstResponder];
    }

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == self.txtCountry) {
        return NO;
    }
    return YES;
}
    
#pragma mark - RevealController Delegates
- (BOOL)revealControllerPanGestureShouldBegin:(SWRevealViewController *)revealController{
    self.defaults = [NSUserDefaults standardUserDefaults];
    NSString *val = [self.defaults valueForKey:@"LANG"];
    NSLog(@"the language selected %@",val);
    
    if([val isEqualToString:@"Arab"]){
        if(revealController.frontViewPosition == FrontViewPositionLeft){
            return NO;
        }
    }
    return YES;
}
    
#pragma mark - IBAction
- (IBAction)registerNowClicked:(id)sender {
    NSString *errorMsg = @"";
    if(self.txtFullName.text.length == 0 || self.txtFullName.text == NULL){
        errorMsg = @"Full name cannot be blank";
    }
    else if(self.txtMobileNo.text.length == 0 || self.txtMobileNo.text == NULL){
        errorMsg = @"Mobile No cannot be blank";
    }
    else if(self.txtCompany.text.length == 0 || self.txtCompany.text == NULL){
        errorMsg = @"Company Name cannot be blank";
    }
    else if(self.txtEmailID.text.length == 0 || self.txtEmailID.text == NULL){
        errorMsg = @"Category cannot be blank";
    }
    else if(self.txtCountry.text.length == 0 || self.txtCountry.text == NULL){
        errorMsg = @"Country cannot be blank";
    }
    
    
    if(errorMsg.length == 0){
        //NSString *url = [NSString stringWithFormat:@"http://www.azanintl.com/Service1.svc/OtpGeneration?mobileNo=%@",self.mobileNoTextField.text];
        NSString *commentStr = self.txtViewComment.text;
        if([self.txtViewComment.text isEqualToString:@"Comment"])
        commentStr = @"";
        
        NSString *url = [NSString stringWithFormat:@"http://www.azanintl.com/Service1.svc/EmployerRegistration?Language=%@&FullName=%@&CompanyName=%@&EmailId=%@&MobileNo=%@&Country=%@&Comment=%@",[self getLang],self.txtFullName.text,self.txtCompany.text,self.txtEmailID.text,self.txtMobileNo.text,self.txtCountry.text,commentStr];
        [self callOTPApi:url andWithTag:@"REG"];
    }else{
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Employer" message:errorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    
}

#pragma mark - Connection Delegates..
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.webResponseData  appendData:data];
}
    
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"Some error in your Connection. Please try again.");
    [HUD hideAnimated:YES];
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Employer" message:@"Issue with server, Please Try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    alert.tag = 6;
    [alert show];
}
    
-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSLog(@"Received Bytes from server: %lu", (unsigned long)[self.webResponseData length]);
    NSError *error;
    NSDictionary *JSONDictionary = [NSJSONSerialization JSONObjectWithData:self.webResponseData
                                                                   options: NSJSONReadingMutableContainers
                                                                     error: &error];
    [HUD hideAnimated:YES];
    NSLog(@"dictionary is %@",JSONDictionary);
    if(connection == theConnection1){
        self.responseDict = [JSONDictionary objectForKey:@"OtpGenerationResult"];
        BOOL flag = [[self.responseDict objectForKey:@"flag"] boolValue];
        if(flag){
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Hello!" message:@"Please enter OTP" delegate:self cancelButtonTitle:@"Verify" otherButtonTitles:nil];
            alert.tag = 1;
            alert.alertViewStyle = UIAlertViewStylePlainTextInput;
            UITextField * alertTextField = [alert textFieldAtIndex:0];
            alertTextField.keyboardType = UIKeyboardTypeNumberPad;
            //alertTextField.placeholder = @"Enter OTP";
            alertTextField.text = [self.responseDict objectForKey:@"Otp"];
            [alert show];
        }
    }else if(connection == theConnection2){
        self.responseDict = [JSONDictionary objectForKey:@"OtpVerificationResult"];
        BOOL flag = [[self.responseDict objectForKey:@"flag"] boolValue];
        if(flag){
            NSLog(@"the OTP VERified successfully");
            
            NSString *url = [NSString stringWithFormat:@"http://www.azanintl.com/Service1.svc/EmployerRegistration?Language=%@&FullName=%@&CompanyName=%@&EmailId=%@&MobileNo=%@&Country=%@&Comment=%@",[self getLang],self.txtFullName.text,self.txtCompany.text,self.txtEmailID.text,self.txtMobileNo.text,self.txtCountry.text,self.txtViewComment.text];
            
            [self callOTPApi:url andWithTag:@"REG"];
            
        }else{
            NSLog(@"THE OTP FAILED");
        }
    }else{
        self.responseDict = [JSONDictionary objectForKey:@"EmployerRegistrationResult"];
        int status = [[self.responseDict objectForKey:@"Status"] intValue];
        if(status == 1){
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Employer" message:@"Registered Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            alert.tag = 2;
            [alert show];
            [self clearAllFields];
        }
    }
}

#pragma mark - Alert view delegates
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 1){
        NSLog(@"Entered: %@",[[alertView textFieldAtIndex:0] text]);
        
        NSLog(@"the data is %@",self.responseDict);
        
        NSString *url = [NSString stringWithFormat:@"http://www.azanintl.com/Service1.svc/OtpVerification?mobileNo=%@&OtpCode=%@&userId=%@",self.txtMobileNo.text,[[alertView textFieldAtIndex:0] text],[self.responseDict objectForKey:@"tempUserId"]];
        
        [self callOTPApi:url andWithTag:@"OTPVAL"];
    }
}
    
#pragma mark - picker
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return  1;
}
    
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.countryNames.count;
}
    
- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return  CustomLocalisedString([self.countryNames objectAtIndex:row], [self.countryNames objectAtIndex:row]);
}
    
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.txtCountry.text = CustomLocalisedString([self.countryNames objectAtIndex:row], [self.countryNames objectAtIndex:row]);
    [self.txtCountry endEditing:YES];
}
    

    
    
    @end
