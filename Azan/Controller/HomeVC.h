//
//  HomeVC.h
//  
//
//  Created by Ashwin on 02/07/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
@import AVFoundation;
@import AVKit;
#import "Constants.h"
#import "LanguageManager.h"
#import "ViewController.h"

@interface HomeVC : UIViewController<SWRevealViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, nonatomic) IBOutlet UINavigationItem *navItem;
@property(nonatomic,retain) NSUserDefaults *defaults;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideMenuBtn;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightSideMenuBtn;
@property (weak, nonatomic) IBOutlet UIView *employerView;
@property (weak, nonatomic) IBOutlet UIView *employeeView;
@property (weak, nonatomic) IBOutlet UILabel *employeeLbl;
@property (weak, nonatomic) IBOutlet UILabel *employerLbl;
@property (weak, nonatomic) IBOutlet UIView *videoView;

@end
