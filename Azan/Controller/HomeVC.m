//
//  HomeVC.m
//  
//
//  Created by Ashwin on 02/07/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "HomeVC.h"

@interface HomeVC (){
    AVPlayerViewController *playerViewController;
    AVPlayer * player;
}

@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self customSetup];
    [self playVideo];
    [self createBarBtns];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

-(void)playVideo{
    NSURL *url1 = [[NSBundle mainBundle] URLForResource:VIDEONAME withExtension:@"mp4"];
    playerViewController = [[AVPlayerViewController alloc] init];
    
    AVURLAsset *asset = [AVURLAsset assetWithURL: url1];
    AVPlayerItem *item = [AVPlayerItem playerItemWithAsset: asset];
    
    player = [[AVPlayer alloc] initWithPlayerItem: item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    
    playerViewController.player = player;
    [playerViewController.view setFrame:CGRectMake(0, 0, self.videoView.bounds.size.width, self.videoView.bounds.size.height)];
    [playerViewController setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    playerViewController.showsPlaybackControls = NO;
    [self.videoView addSubview:playerViewController.view];
    
    [player play];
}

-(void)itemDidFinishPlaying:(NSNotification *) notification {
    // Will be called when AVPlayer finishes playing playerItem
    [self playVideo];
    NSLog(@"play video again");
}

- (void)customSetup
{
    self.employeeLbl.text = CustomLocalisedString(@"Employee", @"Employee");
    self.employerLbl.text = CustomLocalisedString(@"Employer", @"Employer");
    
    //SWRevealViewController *revealViewController = self.revealViewController;
    //revealViewController.delegate = self;
    //self.defaults = [NSUserDefaults standardUserDefaults];
    //NSString *val = [self.defaults valueForKey:@"LANG"];
    //NSLog(@"the language selected %@",val);
    
    
    /*if ( revealViewController )
    {
        if([val isEqualToString:@"Arab"]){
            [self.sideMenuBtn setEnabled:NO];
            [self.sideMenuBtn setTintColor: [UIColor clearColor]];
            [self.rightSideMenuBtn setEnabled:YES];
            [self.rightSideMenuBtn setTintColor: [UIColor whiteColor]];
            
            [self.rightSideMenuBtn setTarget: self.revealViewController];
            self.revealViewController.rightViewRevealWidth = 350;
            [self.rightSideMenuBtn setAction: @selector( rightRevealToggle: )];
            [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        }else{
            [self.sideMenuBtn setEnabled:YES];
            [self.sideMenuBtn setTintColor:[UIColor whiteColor]];
            [self.rightSideMenuBtn setEnabled:YES];
            [self.rightSideMenuBtn setTintColor: [UIColor whiteColor]];
            [self.rightSideMenuBtn setAction:@selector(moveToChangeLangPage)];
            
            [self.sideMenuBtn setTarget: self.revealViewController];
            self.revealViewController.rearViewRevealWidth = 350;
            [self.sideMenuBtn setAction: @selector( revealToggle: )];
            [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        }
    }*/
}

-(void)createBarBtns{
    self.defaults = [NSUserDefaults standardUserDefaults];
    NSString *val = [self.defaults valueForKey:@"LANG"];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.delegate = self;
    
    if([val isEqualToString:@"Arab"]){
        //right side menu..
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [backButton setFrame:CGRectMake(0, 0, 22,17)];
        [ backButton setBackgroundImage:[UIImage imageNamed:@"reveal-icon.png"] forState:UIControlStateNormal];
        [backButton setTintColor:[UIColor whiteColor]];
        [backButton addTarget:self.revealViewController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithCustomView:backButton];
        self.navItem.rightBarButtonItem = back;
        
        //left home btn..
        UIButton *homeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [homeBtn setFrame:CGRectMake(0, 0, 30,25)];
        [ homeBtn setBackgroundImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
        [homeBtn setTintColor:[UIColor whiteColor]];
        [homeBtn addTarget:self action:@selector(moveToChangeLangPage) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *home = [[UIBarButtonItem alloc]initWithCustomView:homeBtn];
        self.navItem.leftBarButtonItem = home;
        
    }
    else{
        //left side menu..
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [backButton setFrame:CGRectMake(0, 0, 22,17)];
        [ backButton setBackgroundImage:[UIImage imageNamed:@"reveal-icon.png"] forState:UIControlStateNormal];
        [backButton setTintColor:[UIColor whiteColor]];
        [backButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithCustomView:backButton];
        self.navItem.leftBarButtonItem = back;
        
        //home btn
        UIButton *homeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [homeBtn setFrame:CGRectMake(0, 0, 30,25)];
        [ homeBtn setBackgroundImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
        [homeBtn setTintColor:[UIColor whiteColor]];
        [homeBtn addTarget:self action:@selector(moveToChangeLangPage) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *home = [[UIBarButtonItem alloc]initWithCustomView:homeBtn];
        self.navItem.rightBarButtonItem = home;
        
        
    }
    self.revealViewController.rearViewRevealWidth = 350;
    self.revealViewController.rightViewRevealWidth = 350;
    [self.navBar addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

-(void)moveToChangeLangPage{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:nil];
    ViewController *add =
    [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    
    [self presentViewController:add
                       animated:YES
                     completion:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [player seekToTime:CMTimeMake(0, 1)];
    [player pause];
}

#pragma mark state preservation / restoration

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Save what you need here
    
    [super encodeRestorableStateWithCoder:coder];
}


- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Restore what you need here
    
    [super decodeRestorableStateWithCoder:coder];
}


- (void)applicationFinishedRestoringState
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Call whatever function you need to visually restore
    [self customSetup];
}

- (BOOL)revealControllerPanGestureShouldBegin:(SWRevealViewController *)revealController{
    self.defaults = [NSUserDefaults standardUserDefaults];
    NSString *val = [self.defaults valueForKey:@"LANG"];
    NSLog(@"the language selected %@",val);

    if([val isEqualToString:@"Arab"]){
        if(revealController.frontViewPosition == FrontViewPositionLeft){
            return NO;
        }
    }
    return YES;
}

@end
