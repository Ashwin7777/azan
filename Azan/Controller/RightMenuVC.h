//
//  RightMenuVC.h
//  Azan
//
//  Created by Ashwin on 09/07/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "LanguageManager.h"

@interface RightMenuVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *rightsideMenuTableView;

@end
