//
//  RightMenuVC.m
//  Azan
//
//  Created by Ashwin on 09/07/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "RightMenuVC.h"

@interface RightMenuVC ()

@end

@implementation RightMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.rightsideMenuTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    
    NSArray *arr = [[NSArray alloc] initWithObjects:@"Employer",@"Employee",@"About Us",@"Why Us",@"Services",@"Contact Us",@"Share", nil];
    
    switch ( indexPath.row )
    {
        case 0:
            CellIdentifier = @"employer";
            break;
        case 1:
            CellIdentifier = @"employee";
            break;
        case 2:
            CellIdentifier = @"About";
            break;
        case 4:
            CellIdentifier = @"services";
            break;
        case 5:
            CellIdentifier = @"contact";
            break;
        case 3:
            CellIdentifier = @"why";
            break;
        case 6:
            CellIdentifier = @"share";
            break;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier forIndexPath: indexPath];
    UILabel *lbl = [cell viewWithTag:indexPath.row + 1];
    NSString *val = [arr objectAtIndex:indexPath.row];
    lbl.text = CustomLocalisedString(val, val);
    
    return cell;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
