//
//  SideMenuVC.h
//  
//
//  Created by Ashwin on 02/07/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AboutUSVC.h"
#import "ContactUsVC.h"
#import "ServicesVC.h"
#import "LanguageManager.h"
#import "Constants.h"

@interface SideMenuVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *sideMenuTableView;


@end
