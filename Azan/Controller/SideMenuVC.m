//
//  SideMenuVC.m
//  
//
//  Created by Ashwin on 02/07/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "SideMenuVC.h"

@interface SideMenuVC ()

@property(nonatomic, retain) NSMutableArray *arrOfMenus;
@property(nonatomic, retain) NSMutableArray *arrOfMenuImages;

@end

@implementation SideMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   // self.arrOfMenus = [[NSMutableArray alloc] initWithObjects:@"Employer",@"Employee",@"About Us",@"Services",@"Contact Us",@"Share" ,nil];
    //self.arrOfMenuImages = [[NSMutableArray alloc] initWithObjects:@"emplyer.png",@"employee.png",@"about.png",@"services.png",@"contact.png",@"share.png", nil];
    //[self.sideMenuTableView registerNib:@"" forCellReuseIdentifier:@"Cell"];
    self.sideMenuTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    NSArray *arr = [[NSArray alloc] initWithObjects:@"Employer",@"Employee",@"About Us",@"Why Us",@"Services",@"Contact Us",@"Share", nil];
    
    switch ( indexPath.row )
    {
        case 0:
            CellIdentifier = @"employer";
            break;
        case 1:
            CellIdentifier = @"employee";
            break;
        case 2:
            CellIdentifier = @"About";
            break;
        case 4:
            CellIdentifier = @"services";
            break;
        case 5:
            CellIdentifier = @"contact";
            break;
        case 3:
            CellIdentifier = @"why";
            break;
        case 6:
            CellIdentifier = @"share";
            break;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier forIndexPath: indexPath];
    NSString *str = [arr objectAtIndex:indexPath.row];
    cell.textLabel.text = CustomLocalisedString(str,str);
    
    return cell;
    
}

/*
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.arrOfMenus count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.textLabel.text = [self.arrOfMenus objectAtIndex:indexPath.row];
    cell.imageView.image = [UIImage imageNamed:[self.arrOfMenuImages objectAtIndex:indexPath.row]];
    
    return cell;
    
}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"AZAN";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            break;
        case 1:
            
            break;
        case 2:{
            AboutUSVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUSVC"];
            [self.navigationController pushViewController:vc animated:YES];
        }
            
            break;
        case 3:
            
            break;
        case 4:
            
            break;
        case 5:
            
            break;
        case 6:
            
            break;
        default:
            break;
    }
}*/

@end
