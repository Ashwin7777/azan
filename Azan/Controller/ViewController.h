//
//  ViewController.h
//  
//
//  Created by Ashwin on 26/06/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageManager.h"
#import "SWRevealViewController.h"
@import AVFoundation;
@import AVKit;

@interface ViewController : UIViewController

- (IBAction)ArabicClicked:(id)sender;
- (IBAction)EngClicked:(id)sender;
- (IBAction)HindiClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *videoView;

@end

