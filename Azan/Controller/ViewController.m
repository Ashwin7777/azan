//
//  ViewController.m
//  
//
//  Created by Ashwin on 26/06/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "ViewController.h"
#import "Constants.h"

@interface ViewController (){
    AVPlayerViewController *playerViewController;
    AVPlayer * player;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self playVideo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [player seekToTime:CMTimeMake(0, 1)];
    [player pause];
}

-(void)playVideo{
    NSURL *url1 = [[NSBundle mainBundle] URLForResource:VIDEONAME withExtension:@"mp4"];
    playerViewController = [[AVPlayerViewController alloc] init];
    
    AVURLAsset *asset = [AVURLAsset assetWithURL: url1];
    AVPlayerItem *item = [AVPlayerItem playerItemWithAsset: asset];
    
    player = [[AVPlayer alloc] initWithPlayerItem: item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    
    playerViewController.player = player;
    [playerViewController.view setFrame:CGRectMake(0, 0, self.videoView.bounds.size.width, self.videoView.bounds.size.height)];
    [playerViewController setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    playerViewController.showsPlaybackControls = NO;
    [self.videoView addSubview:playerViewController.view];
    
    [player play];
}

-(void)itemDidFinishPlaying:(NSNotification *) notification {
    // Will be called when AVPlayer finishes playing playerItem
    [self playVideo];
}

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    UIButton *btn = (UIButton *)sender;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(btn.tag == 1){
        [defaults setValue:@"Arab" forKey:@"LANG"];
    }else if(btn.tag == 2){
        [defaults setValue:@"Eng" forKey:@"LANG"];
    }else if(btn.tag == 3){
        [defaults setValue:@"Hind" forKey:@"LANG"];
    }
    [defaults synchronize];        
}


#pragma mark -- IBACTIONS
- (IBAction)ArabicClicked:(id)sender {
    LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
    [languageManager setLanguageWithLocale:languageManager.availableLocales[0]];
}

- (IBAction)EngClicked:(id)sender {
    LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
    [languageManager setLanguageWithLocale:languageManager.availableLocales[1]];
}

- (IBAction)HindiClicked:(id)sender {
    LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
    [languageManager setLanguageWithLocale:languageManager.availableLocales[2]];
}
@end
