//
//  WhyUsVC.h
//  Azan
//
//  Created by Ashwin on 15/08/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "ViewController.h"

@interface WhyUsVC : UIViewController<SWRevealViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, nonatomic) IBOutlet UINavigationItem *navItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideMenuBtn;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightSideMenuBtn;
@property (weak, nonatomic) IBOutlet UITextView *txtViewWhy;
@property(nonatomic,retain) NSUserDefaults *defaults;
@end
