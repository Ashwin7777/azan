//
//  About.h
//  Azan
//
//  Created by Ashwin on 25/07/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface About : NSObject

@property (nonatomic,retain) NSString *Description;
@property (nonatomic,retain) NSString *Language;
@property (nonatomic,retain) NSString *Page;
@property (nonatomic,retain) NSString *Status;


@end
