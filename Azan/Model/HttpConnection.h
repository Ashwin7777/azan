//
//  HttpConnection.h
//  connSingleClass
//
//  Created by admin on 10/02/16.
//  Copyright © 2016 ashwin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HttpConnection : NSObject

typedef void (^callBackBlock)(NSData* response,int statusCode,NSError *err);

-(void) postToUrl: (NSString*) url withBody: (NSDictionary *) param
     withCallback: (callBackBlock) callback;

@end
