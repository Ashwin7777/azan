//
//  HttpConnection.m
//  connSingleClass
//
//  Created by admin on 10/02/16.
//  Copyright © 2016 ashwin. All rights reserved.
//

#import "HttpConnection.h"

@implementation HttpConnection

-(void) postToUrl: (NSString*) url withBody: (NSDictionary *) param
     withCallback: (callBackBlock) callback{
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSMutableURLRequest *request = [self createRequest:url andWithData:param];
    /*NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url1
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    //NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"9503556812", @"Mobile No",                           nil];
    //NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    [request setHTTPBody:body];
    
    [[session dataTaskWithURL:url1 completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse *)response;
        NSInteger statusCode = [HTTPResponse statusCode];
        NSLog(@"status is %d",(int)statusCode);
        callback(data,(int)statusCode,error);
    }] resume];*/
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse *)response;
        NSInteger statusCode = [HTTPResponse statusCode];
        NSLog(@"status is %d",(int)statusCode);
        callback(data,(int)statusCode,error);
    }];
    [postDataTask resume];

}

-(NSMutableURLRequest *)createRequest:(NSString *)_url andWithData:(NSDictionary *)paramters{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:_url]];
    
    [request setHTTPMethod:@"POST"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSString *bound = [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];

    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", bound];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    
    NSMutableData *httpBody = [NSMutableData data];
    if (paramters != NULL) {
        [paramters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
            [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", bound] dataUsingEncoding:NSUTF8StringEncoding]];
            [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
            [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
        }];
    }
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", bound] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:httpBody];
    
    return request;
}

@end
